directory '/var/log/nginx/mplk-site' do
  action :create
  owner  'root'
  group  'root'
  mode   '0755'
  recursive true
end
