include_recipe 'nginx::default'

template '/etc/nginx/sites-available/mplk-site.conf' do
  source 'mplk-site.erb'
  owner  'root'
  group  'root'
  mode   '0644'
end

link '/etc/nginx/sites-enabled/mplk-site.conf' do
  to '/etc/nginx/sites-available/mplk-site.conf'
  notifies :restart, 'service:[nginx]'
end
