#
# Cookbook Name:: mplk-site
# Recipe:: default
#
# Copyright 2016, Lbs Local
#
# All rights reserved - Do Not Redistribute
#

include_recipe 'mplk-site::setup'
include_recipe 'mplk-site::vhost'
