name             'mplk-site'
maintainer       'Lbs Local'
maintainer_email 'infra@lbslocal.com'
license          'All rights reserved'
description      'Installs/Configures mplk-site'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
issues_url 'https://github.com/chef-cookbooks/something/issues'
source_url 'https://github.com/chef-cookbooks/something/issues'

depends 'apt'
depends 'nginx'
depends 'nodejs'
